package cz.muni.fi.pv256.zina1.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cz.muni.fi.pv256.zina1.ui.FriendsFragment;
import cz.muni.fi.pv256.zina1.ui.RecentMessagesFragment;


/**
 * Created by Wendy on 18.8.2014.
 */
public class PagerAdapter extends FragmentPagerAdapter
{

    public PagerAdapter(FragmentManager fm)
    {
        super(fm);
    }

    @Override public Fragment getItem(int position)
    {
        switch (position)
        {
        case 0:
            return new RecentMessagesFragment();
        case 1:
            return new FriendsFragment();

        }
        return null;
    }

    @Override public int getCount()
    {
        return 2;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position)
    {
        switch (position % 2)
        {
        case 0:
            return "Messages";
        case 1:
            return "Friends";
        default:
            return "Unknown fragment";
        }
    }
}
