package cz.muni.fi.pv256.zina1.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.muni.fi.pv256.zina1.R;


/**
 * Created by Wendy on 18.8.2014.
 */
public class RecentMessagesFragment extends Fragment
{

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// L I F E C Y C L E    M E T H O D S //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override public void onCreate(Bundle savedInstanceState)
    {
        Log.i("", "RecentMessagesFragment onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_recent_msgs, container, false);
        return view;
    }
}
