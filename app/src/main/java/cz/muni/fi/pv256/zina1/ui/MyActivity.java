package cz.muni.fi.pv256.zina1.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import cz.muni.fi.pv256.zina1.R;
import cz.muni.fi.pv256.zina1.util.ZinaUtil;

public class MyActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks
{
    private Fragment loginFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence             mTitle;
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private static final String EXTRA_THEME = "theme";


    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// L I F E C Y C L E    M E T H O D S //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(ZinaUtil.TAG, "[MyActivity]::[onCreate]");
        super.onCreate(savedInstanceState);

        int theme = getIntent().getIntExtra(EXTRA_THEME, 1);
        if (theme == 0)
        {
            setTheme(R.style.AppTheme_Lime);
        }
        if (theme == 1)
        {
            setTheme(R.style.AppTheme_Yellow);
        }

        setContentView(R.layout.activity_my);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// O P T I O N S    M E N U ////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.d(ZinaUtil.TAG, "[MainActivity]::[onCreateOptionsMenu]");

        getMenuInflater().inflate(R.menu.global, menu);

        int theme = getIntent().getIntExtra(EXTRA_THEME, 1);
        if (theme == 0)
        {
            menu.removeItem(R.id.action_lime);
        }
        if (theme == 1)
        {
            menu.removeItem(R.id.action_yellow);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.d(ZinaUtil.TAG, "[MainActivity]::[onOptionsItemSelected]");

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        return super.onOptionsItemSelected(item);
        int theme = -1;
        switch (item.getItemId())
        {
        case R.id.action_lime:
            theme = 0;
            break;
        case R.id.action_yellow:
            theme = 1;
            break;
        default:
            return false;
        }

        Intent intent = new Intent(this, MyActivity.class);
        intent.putExtra("theme", theme);
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

//        ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(this, R.anim.abc_fade_in, R.anim.abc_fade_out);
//        Bundle bundle = activityOptions.toBundle();
//        startActivity(intent,bundle);
        finish();

        return true;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// N A V I G A T I O N    D R A W E R //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public void onSectionAttached(int number)
    {
        Log.d(ZinaUtil.TAG, "[MainActivity]::[onSectionAttached]::[" + number + "]");
        switch (number)
        {
        case 1:
            mTitle = getString(R.string.title_section1);
            break;
        case 2:
            mTitle = getString(R.string.title_section2);
            break;
        }
    }

    @Override public void onNavigationDrawerItemSelected(int position)
    {
        Log.d(ZinaUtil.TAG, "[MyActivity]::[onNavigationDrawerItemSelected() " + position + "]");

        String fragmentTitle = "";
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (position)
        {
        case 0:
            fragmentTitle = getResources().getString(R.string.title_section1);
            fragment = fragmentManager.findFragmentByTag(
                    fragmentTitle);
            if (fragment == null)
            {
                Log.i("Zina", "MyActivity creating new MessagesFragment");
                fragment = new MessagesFragment();

                fragmentManager.beginTransaction()
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.container, fragment, fragmentTitle)
                        .commit();
            }
            break;
        case 1:
            fragmentTitle = getResources().getString(R.string.title_section2);
            fragment = fragmentManager.findFragmentByTag(fragmentTitle);
            if (fragment == null)
            {
                Log.i("Zina", "MyActivity creating new MapFragment");
                fragment = new MapFragment();
                fragmentManager.beginTransaction()
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.container, fragment, fragmentTitle)
                        .commit();
            }
            break;
        }

        // update the main content by replacing fragments
        Log.d(ZinaUtil.TAG, "[MyActivity]::[onNavigationDrawerItemSelected() " + position + "]::[replacing fragment]");


    }

    public boolean isDrawerOpen()
    {
        if (mNavigationDrawerFragment == null)
        {
            Log.w(ZinaUtil.TAG,"[MyActivity]::[isDrawerOpen]::[drawer is null]");
            return false;
        }

        return mNavigationDrawerFragment.isDrawerOpen();
    }

    public void closeDrawer()
    {
        mNavigationDrawerFragment.close();
    }

    public void setDrawerToggleEnabled(boolean enabled)
    {
        mNavigationDrawerFragment.setDrawerToggleEnabled(enabled);
    }

}
