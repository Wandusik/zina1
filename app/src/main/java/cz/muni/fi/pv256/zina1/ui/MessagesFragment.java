package cz.muni.fi.pv256.zina1.ui;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import cz.muni.fi.pv256.zina1.R;
import cz.muni.fi.pv256.zina1.adapter.PagerAdapter;
import cz.muni.fi.pv256.zina1.util.ZinaUtil;


/**
 * Created by Wendy on 18.8.2014.
 */
public class MessagesFragment extends Fragment
{

    private static final String TAB      = "tab";
    private static final String POSITION = "position";

    private ViewPager    pager;
    private PagerAdapter pagerAdapter;
    private PagerSlidingTabStrip tabs;


    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// L I F E C Y C L E    M E T H O D S //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.i("", "onCreate");
//        setRetainInstance(true);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.i(ZinaUtil.TAG, "[MessagesFragment]::[onCreateView]");
        View view = inflater.inflate(R.layout.fragment_messages, container, false);

        return view;
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState)
    {
        Log.d(ZinaUtil.TAG, "[MessagesFragment]::[onViewCreated]");

        pagerAdapter = new PagerAdapter(getChildFragmentManager());

        pager = (ViewPager) view.findViewById(R.id.viewpager);
        pager.setAdapter(pagerAdapter);
        pagerAdapter.notifyDataSetChanged();

        //For pagerSlidingTabStrip, comment if using ActionBar tabs
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        tabs.setShouldExpand(!getActivity().getResources().getBoolean(R.bool.isTablet));
        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override public void onPageScrolled(int i, float v, int i2)
            {

            }

            @Override public void onPageSelected(int i)
            {
                Log.i(ZinaUtil.TAG, "[MessagesFragment]::[onPageSelected]::[" + i + "]");

                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(TAB, 0);
                sharedPreferences.edit().putInt(POSITION, i).commit();
            }

            @Override public void onPageScrollStateChanged(int i)
            {
            }
        });


        //Do things to keep state after rotation
        int position;
        if (savedInstanceState != null)
        {
            position = savedInstanceState.getInt(TAB);
        }
        else
        {
            position = getActivity().getSharedPreferences(TAB, 0).getInt(POSITION, 0);
        }

        pager.setCurrentItem(position);

        // Used for ActionBar tabs. Comment out to see the behavior
//        recreateTabs();
//        if (savedInstanceState != null)
//        {
//            ((MyActivity) getActivity()).getSupportActionBar().setSelectedNavigationItem(savedInstanceState.getInt(TAB));
//        }

        super.onViewCreated(view, savedInstanceState);

    }

    @Override public void onActivityCreated(Bundle savedInstanceState)
    {
        Log.i(ZinaUtil.TAG, "[MessagesFragment]::[onActivityCreated]");

        super.onActivityCreated(savedInstanceState);
    }

    @Override public void onSaveInstanceState(Bundle outState)
    {
        Log.i(ZinaUtil.TAG, "[MessagesFragment]::[onSaveInstanceState]");
        super.onSaveInstanceState(outState);

        if (pager != null)
        {
            // Used for PagerSlidingTabStrip ,comment if ActionBar tabs used
            int current = pager.getCurrentItem();

            // Used for ActionBar tabs. Comment out to see the behavior
//            int current = ((MyActivity) getActivity()).getSupportActionBar().getSelectedNavigationIndex();
            outState.putInt(TAB, current);
            Log.i(ZinaUtil.TAG, "[MessagesFragment]::[onSaveInstanceState]::[" + current + "]");
        }
        else
        {
            Log.w(ZinaUtil.TAG, "[MessagesFragment]::[onSaveInstanceState]::[Pager is null]");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// H E L P E R    M E T H O D S ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    //ActionBar tabs
    private void recreateTabs()
    {
        Log.i(ZinaUtil.TAG, "[MessagesFragment]::[recreateTabs]");
        final ActionBar actionBar = ((MyActivity) getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.TabListener tabListener = new ActionBar.TabListener()
        {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                Log.i(ZinaUtil.TAG, "[MessagesFragment]::[recreateTabs]::[onTabSelected]::[" + tab.getPosition() + "]");

                if (pager != null)
                {
                    pager.setCurrentItem(tab.getPosition());
                }

                MyActivity activity = ((MyActivity) getActivity());
                if (activity != null)
                {
                    if (activity.isDrawerOpen())
                    {
                        activity.closeDrawer();
                    }
                    else
                    {
                        Log.w(ZinaUtil.TAG, "[MessagesFragment]::[recreateTabs]::[onTabSelected]::[drawer is not open]");
                    }
                }
                else
                {
                    Log.w(ZinaUtil.TAG, "[MessagesFragment]::[recreateTabs]::[onTabSelected]::[activity is null]");
                }
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                // probably ignore this event
            }
        };

        actionBar.removeAllTabs();
        ActionBar.Tab tab = actionBar.newTab().setText("Recent").setTabListener(tabListener);
        actionBar.addTab(tab);
        ActionBar.Tab tab1 = actionBar.newTab().setText("Friends").setTabListener(tabListener);
        actionBar.addTab(tab1);

        pager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener()
                {
                    @Override
                    public void onPageSelected(int position)
                    {
                        // When swiping between pages, select the
                        // corresponding tab.
                        getActionBar().setSelectedNavigationItem(position);
                    }
                });
    }

    private ActionBar getActionBar()
    {
        return ((MyActivity) getActivity()).getSupportActionBar();
    }

}
