package cz.muni.fi.pv256.zina1.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.muni.fi.pv256.zina1.R;
import cz.muni.fi.pv256.zina1.util.ZinaUtil;

/**
 * Created by Wendy on 18.8.2014.
 */
public class FriendsFragment extends Fragment
{
    private int duration;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// L I F E C Y C L E    M E T H O D S //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.i(ZinaUtil.TAG, "[FriendsFragment]::[onCreateView]");
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        return view;
    }
}
