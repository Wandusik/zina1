package cz.muni.fi.pv256.zina1.ui;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.muni.fi.pv256.zina1.BuildConfig;
import cz.muni.fi.pv256.zina1.R;


/**
 * Created by Wendy on 18.8.2014.
 */
public class MapFragment extends Fragment
{
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// L I F E C Y C L E    M E T H O D S //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        setNavigationMode();

        return view;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////// P R I V A T E    M E T H O D S //////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @SuppressLint("NewApi") private void setNavigationMode()
    {
        //when ActionBar.Tabs are used...
        if (BuildConfig.VERSION_CODE > 10)
        {
            getActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
        else
        {
            ((MyActivity) getActivity()).getSupportActionBar().setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_STANDARD);
        }
    }
}
